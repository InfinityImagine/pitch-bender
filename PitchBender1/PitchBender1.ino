//*********** PITCH BENDER MARK 1 ************************//
#include <MIDI.h>
#include <LiquidCrystal.h>
//#include <NewPing.h>

// pin A2 is free
//pin 12 and 13 are free
//pins 0 and 1 used for serial communications (TX and RX)

#define TRIGGER_PIN  A3 
#define ECHO_PIN     A4 
#define MAX_DISTANCE 50

#define RSPin 7
#define EnablePin 6
#define D4Pin 5
#define D5Pin 4
#define D6Pin 3
#define D7Pin 2

#define rightPin 8
#define downPin 9
#define upPin 10
#define leftPin 11

#define xPin A0
#define yPin A1
#define buttonPin A5

#define menuMax 19 //ie 20

enum menuCode
{
       maxRange,
       upMode,
       upRange,
       upChan,
       lowMode,
       lowRange,
       lowChan,
       yCont,
       rightMode,
       rightRange,
       rightChan,
       leftMode,
       leftRange,
       leftChan,
       xCont,
       dlMode,
       dlRange,
       dlChan,
       save,
       load
};

MIDI_CREATE_DEFAULT_INSTANCE();
//NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
LiquidCrystal lcd(RSPin, EnablePin, D4Pin, D5Pin, D6Pin, D7Pin);

const char* const title[]={"MAX P.B. RANGE", "UPPER MODE", "UPPER RANGE", "UPPER CHANNEL",
                "LOWER MODE", "LOWER RANGE", "LOWER CHANNEL", "Y CONTROLLER",
                "RIGHT MODE", "RIGHE RANGE", "RIGHT CHANNEL",
                "LEFT MODE", "LEFT RANGE", "LEFT CHANNEL", "X CONTROLLER",
                "D-LIGHT MODE", "D-LIGHT RANGE", "D-LIGHT CHANNEL",
                "SAVE PRESET", "LOAD PRESET"};
                
const char* const content[menuMax+1][14]={{"\0"},
                         {"0. Controller", "1. Pitch Bend"}, {"\0"}, {"\0"},
                         {"0. Controller", "1. Pitch Bend"}, {"\0"}, {"\0"},
                         {"0. Split", "1. Common"},
                         {"0. Controller", "1. Pitch Bend"}, {"\0"}, {"\0"},
                         {"0. Controller", "1. Pitch Bend"}, {"\0"}, {"\0"},
                         {"0. Split", "1. Common"},
                         {"0. Controller", "1. Pitch Bend"}, {"\0"}, {"\0"},
                         {"\0"}, {"\0"}};

int Max[menuMax+1]={23, 1, /*127, 127, 126,*/ 23, 15, 1, /*127, 127, 126,*/ 23, 15, 1, 1, /*127, 127, 126,*/ 23, 15, 1, /*127, 127, 126,*/ 23, 15, 1, 1, /*127, 127, 126,*/ 23, 15, 20, 20};
int xMax=500, xMin=500, yMax=500, yMin=500, buff=0;
int xVal=0, yVal=0, dlVal=0;
int xPre=-1, yPre=-1, dlPre=-1;
int menuState=0, preMenu = -1;
//int valueState[menuMax+1]={11, 1, 6, 0, 1, 1, 0, 0, 1, 1, 0, 1, 11, 0, 0, 1, 6, 0, 0, 0};
//int valueState[menuMax+1]={1, 1, 15, 127, 0, 0, 1, 1, 16, 127, 0, 0, 0, 1, 1, 17, 127, 0, 0, 1, 1, 18, 127, 0, 0, 0, 1, 1, 29, 127, 0, 0, 0, 0};
int valueState[menuMax+1]={1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0};
int preValue[menuMax+1]={-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
int chanMsg[16]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int chanPre[16]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
int preButton[5] = {0, 0, 0, 0, 0};

int isPress(int pin)
{
  int ret=0;
  if(pin>12) pin=12;
  int reading = digitalRead(pin);
  if (preButton[pin-rightPin]==LOW && reading==HIGH)
                    ret= 1;
  preButton[pin-rightPin] = reading;
  return ret;
}

void setup()
{
      MIDI.begin(MIDI_CHANNEL_OMNI);
      Serial.begin(115200);
      lcd.begin(16, 2);
      pinMode(upPin, INPUT);
      pinMode(downPin, INPUT);
      pinMode(leftPin, INPUT);
      pinMode(rightPin, INPUT);
      pinMode(buttonPin, INPUT);
      lcd.print("PITCH BENDER M-1");
      lcd.setCursor(0, 1);
      lcd.print("<press up...>");
      while(!isPress(upPin));
      lcd.clear();
      lcd.print("CALIBRATE");
      lcd.setCursor(0, 1);
      lcd.print("JOYSTICK");
      while(!isPress(upPin))
      {
          if(analogRead(A0)>xMax) xMax=analogRead(A0);
          else if(analogRead(A0)<xMin) xMin=analogRead(A0);
          if(analogRead(A1)>yMax) yMax=analogRead(A1);
          else if(analogRead(A1)<yMin) yMin=analogRead(A1);
      }
      ++xMax;
      --xMin;
      ++yMax;
      //--yMin;
      lcd.clear();
}

void loop()
{
      //Buttons 
      if(isPress(leftPin) == 1)
      {
            --valueState[menuState];
            if(valueState[menuState]<0) valueState[menuState]=0;
      }
      if(isPress(rightPin) == 1)
      {
            ++valueState[menuState];
            if(valueState[menuState]>Max[menuState]) valueState[menuState]=Max[menuState];
      }
      if(menuState==maxRange && valueState[menuState]!=preValue[menuState])
      {
              //Max range update.
              Max[upRange]=Max[lowRange]=Max[rightRange]=Max[leftRange]=Max[dlRange]=valueState[maxRange];
              if(valueState[upRange]>Max[upRange]) valueState[upRange]=Max[upRange];
              if(valueState[lowRange]>Max[lowRange]) valueState[lowRange]=Max[lowRange];
              if(valueState[rightRange]>Max[rightRange]) valueState[rightRange]=Max[rightRange];
              if(valueState[leftRange]>Max[leftRange]) valueState[leftRange]=Max[leftRange];
              if(valueState[dlRange]>Max[dlRange]) valueState[dlRange]=Max[dlRange];
      }
      if(isPress(upPin) == 1)
      {
            ++menuState;
            if(menuState>menuMax) menuState=0;
      }
      if(isPress(downPin) == 1)
      {
            --menuState;
            if(menuState<0) menuState=menuMax;
      }
      //lcd print
      if(preMenu != menuState || preValue[menuState] != valueState[menuState])
      {
            preMenu=menuState;
            preValue[menuState]=valueState[menuState];
            lcd.clear();
            lcd.print(title[menuState]);
            lcd.setCursor(0, 1);
            if(content[menuState][0]!="\0")lcd.print(content[menuState][valueState[menuState]]);
            else lcd.print(valueState[menuState]+1);
      }
      //input
      buff = analogRead(xPin);
      xVal = map(buff, xMin, xMax, -8192, 8191);
      //xVal=buff*16-8192;
      buff = analogRead(yPin);
      yVal = map(buff, yMin, yMax, -8192, 8191);
      //yVal=buff*16-8192;
      yVal *=-1;
      //buff = sonar.ping()/US_ROUNDTRIP_CM;
      //if(buff<5 || buff>50) buff = 5;
      //dlVal = map(buff, 5, 48, 0, 8191);
      
      //pitch bend    //WORKING but change to ELLIPSE/DIAMOND
      for(int i=0; i<16; ++i)
      {
          chanMsg[i]=0;
          if(valueState[upChan]==i && yVal>0)
                chanMsg[i] += yVal*(valueState[upRange]+1)*valueState[upMode];
          if(valueState[lowChan]==i && yVal<0)
                chanMsg[i] += yVal*(valueState[lowRange]+1)*valueState[lowMode];
          if(valueState[rightChan]==i && xVal>0)
                chanMsg[i] += xVal*(valueState[rightRange]+1)*valueState[rightMode];
          if(valueState[leftChan]==i && xVal<0)
                chanMsg[i] += xVal*(valueState[leftRange]+1)*valueState[leftMode];
          //if(valueState[dlChan]==i)
            //    chanMsg[i] += dlVal*(valueState[dlRange]+1)*valueState[dlMode];
               
          chanMsg[i]/=(valueState[maxRange]+1);
          //if(xVal!=xPre || yVal!=yPre || dlVal!=dlPre)
          if(chanMsg[i]!=chanPre[i])
          { 
               MIDI.sendPitchBend(chanMsg[i], i+1); 
               chanPre[i]=chanMsg[i];
          }
          else MIDI.sendNoteOff(0, 0, 0); //dummy 
      }

      
/*      //pitch bend ellipse/diamond... incorrect formula
      for(int i=0; i<16; ++i)
      {
          chanMsg[i]=0;
          int x, y, d;
          //x/=(valueState[0]+1);
          //y/=(valueState[0]+1);
          if(valueState[3]==i && yVal>0)
          {
                y = yVal*(valueState[2]+1)*valueState[1];
                chanMsg[i] += y*y;
          }
          else if(valueState[6]==i && yVal<0)
          {
                y = yVal*(valueState[5]+1)*valueState[4];
                chanMsg[i] -= y*y;
          }
          if(valueState[10]==i && xVal>0)
          {
                x = xVal*(valueState[9]+1)*valueState[8];
                chanMsg[i] += x*x;
          }
          else if(valueState[13]==i && xVal<0)
          {
                xVal*(valueState[12]+1)*valueState[11];
                chanMsg[i] -= x*x;
          }
          if(valueState[17]==i)
          {
                d = dlVal*(valueState[16]+1)*valueState[15];
                chanMsg[i] += d*d;
          }
          if(chanMsg[i]<0)     
            chanMsg[i]=-(int)sqrt(-chanMsg[i]);
          else chanMsg[i]=(int)sqrt(chanMsg[i]);
          chanMsg[i]/=(valueState[0]+1);
          if(xVal!=xPre || yVal!=yPre || dlVal!=dlPre)
          //if(chanMsg[i]!=chanPre[i])
          { 
               MIDI.sendPitchBend(chanMsg[i], i+1); 
               chanPre[i]=chanMsg[i];
          }
      }
*/
      
      //controller changes     //WORKING  //ADD upCCMX upCCMN usage
      for(int i=0; i<16; ++i)
      {
          if(yVal!=yPre)
          {
              if(valueState[upChan]==i && valueState[upMode]==0 && (valueState[yCont]==1 || yVal>0))
              {
                  MIDI.sendControlChange(16, (yVal + 8191*valueState[yCont])/(64*(1+valueState[xCont])), i);
                  MIDI.sendControlChange(16, (yVal + 8191*valueState[yCont])/(64*(1+valueState[xCont])), i+1); //dummy
              }
              if(valueState[lowChan]==i && valueState[lowMode]==0 && valueState[yCont]==0 && yVal<0)
              {
                  MIDI.sendControlChange(17, (yVal+8191)/64, i);
                  MIDI.sendControlChange(17, (yVal+8191)/64, i+1); //dummy
              }
              //else MIDI.sendNoteOff(0, 0, 0);  //dummy
          }
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          if(xVal!=xPre)
          {     
              if(valueState[rightChan]==i && valueState[rightMode]==0 && (valueState[xCont]==1 || xVal>0))
              {
                  MIDI.sendControlChange(18, (xVal + 8191*valueState[xCont])/(64*(1+valueState[xCont])), i);
                  MIDI.sendControlChange(18, (xVal + 8191*valueState[xCont])/(64*(1+valueState[xCont])), i+1); //dummy
              }
              if(valueState[leftChan]==i && valueState[leftMode]==0 && valueState[xCont]==0 && xVal<0)
              {
                  MIDI.sendControlChange(19, (xVal+8191)/64, i);
                  MIDI.sendControlChange(19, (xVal+8191)/64, i+1); //dummy
              }
              //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          }
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          /*if(dlVal!=dlPre && valueState[dlMode]==0 && valueState[dlChan]==i)
          {
                MIDI.sendControlChange(30, dlVal/128, i);
                MIDI.sendControlChange(30, dlVal/128, i+1); //dummy
          }*/
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
      }
      
      xVal=xPre;
      yVal=yPre;
      dlVal=dlPre;
}
