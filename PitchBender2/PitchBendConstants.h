// pin A2 is free
//pin 12 and 13 are free
//pins 0 and 1 used for serial communications (TX and RX)
#ifndef PITCHBENDCONSTANTS
#define PITCHBENDCONSTANTS

#define TRIGGER_PIN  A3 
#define ECHO_PIN     A4 
#define MAX_DISTANCE 50

#define RSPin 7
#define EnablePin 6
#define D4Pin 5
#define D5Pin 4
#define D6Pin 3
#define D7Pin 2

#define rightPin 8
#define downPin 9
#define upPin 10
#define leftPin 11

#define xPin A0
#define yPin A1
#define buttonPin A5

//menu states use enum?
#define menuMax 38 //ie 39

#define maxRange 0

#define Mode 0
#define CC 1
#define CCMX 2
#define CCMN 3
#define Range 4
#define Chan 5

#define upMode 1
#define upCC 2
#define upCCMX 3
#define upCCMN 4
#define upRange 5
#define upChan 6

#define lowMode 7
#define lowCC 8
#define lowCCMX 9
#define lowCCMN 10
#define lowRange 11
#define lowChan 12

#define yCont 13

#define rightMode 14
#define rightCC 15
#define rightCCMX 16
#define rightCCMN 17
#define rightRange 18
#define rightChan 19

#define leftMode 20
#define leftCC 21
#define leftCCMX 22
#define leftCCMN 23
#define leftRange 24
#define leftChan 25

#define xCont 26

#define dlMode 27
#define dlCC 28
#define dlCCMX 29
#define dlCCMN 30
#define dlRange 31
#define dlChan 32

#define quad1 33
#define quad2 34
#define quad3 35
#define quad4 36

#define save 37
#define load 38

//modes
#define pitchbend 1
#define controller 0

#define split 0
#define common 1

#define diamond 0
#define ellipse 1

#endif
