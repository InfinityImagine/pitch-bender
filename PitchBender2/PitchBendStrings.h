const char title1[] PROGMEM = "Max P.B. Range";
const char title2[] PROGMEM = "Upper Mode";
const char title3[] PROGMEM = "UpperCC";
const char title4[] PROGMEM = "UpperCC Max";
const char title5[] PROGMEM = "UpperCC Min";
const char title6[] PROGMEM = "Upper Range";
const char title7[] PROGMEM = "Upper Channel";
const char title8[] PROGMEM =  "Lower Mode";
const char title9[] PROGMEM =  "LowerCC";
const char title10[] PROGMEM =  "LowerCC Max";
const char title11[] PROGMEM =  "LowerCC Min";
const char title12[] PROGMEM =  "Lower Range";
const char title13[] PROGMEM =  "Lower Channel";
const char title14[] PROGMEM =  "Y Controller";
const char title15[] PROGMEM =  "Right Mode";
const char title16[] PROGMEM =  "RightCC";
const char title17[] PROGMEM =  "RightCC Max";
const char title18[] PROGMEM =  "RightCC Min";
const char title19[] PROGMEM =  "Right Range";
const char title20[] PROGMEM =  "Right Channel";
const char title21[] PROGMEM =  "Left Mode";
const char title22[] PROGMEM =  "LeftCC";
const char title23[] PROGMEM =  "LeftCC Max";
const char title24[] PROGMEM =  "LeftCC Min";
const char title25[] PROGMEM =  "Left Range";
const char title26[] PROGMEM =  "Left Channel";
const char title27[] PROGMEM =  "X Controller";
const char title28[] PROGMEM =  "D-Light Mode";
const char title29[] PROGMEM =  "D-LightCC";
const char title30[] PROGMEM =  "D-LightCC Max";
const char title31[] PROGMEM =  "D-LightCC Min";
const char title32[] PROGMEM =  "D-Light Range";
const char title33[] PROGMEM =  "D-Light Channel";
const char title34[] PROGMEM =  "Quadrant 1";
const char title35[] PROGMEM =  "Quadrant 2";
const char title36[] PROGMEM =  "Quadrant 3";
const char title37[] PROGMEM =  "Quadrant 4";
const char title38[] PROGMEM =  "Save Preset";
const char title39[] PROGMEM =  "Load Preset";

const char* const title[] PROGMEM ={title1, title2, title3, title4, title5, title6, title7, title8, title9, title10,
                                    title11, title12, title13, title14, title15, title16, title17, title18, title19, title20,
                                    title21, title22, title23, title24, title25, title26, title27, title28, title29, title20,
                                    title31, title32, title33, title34, title35, title36, title37, title38, title39};

const char contentControl[] PROGMEM = "0.Controller";
const char contentBend[] PROGMEM = "1.Pitch Bend";
const char* const contentCP[2] PROGMEM = {contentControl, contentBend};

const char contentSplit[] PROGMEM = "0.Split";
const char contentCommon[] PROGMEM = "1.Common";
const char* const contentSC[2] PROGMEM = {contentSplit, contentCommon};

const char contentDiamond[] PROGMEM = "0.Diamond";
const char contentEllipse[] PROGMEM = "1.Ellipse";
const char* const contentQ[2] PROGMEM = {contentDiamond, contentEllipse};

const char contentNull[] PROGMEM = "X";
const char* const noContent[1] PROGMEM = {contentNull};

const char* const* content[menuMax+1] ={ noContent,
                          contentCP, noContent, noContent, noContent, noContent, noContent,
                          contentCP, noContent, noContent, noContent, noContent, noContent,
                          contentSC,
                          contentCP, noContent, noContent, noContent, noContent, noContent,
                          contentCP, noContent, noContent, noContent, noContent, noContent,
                          contentSC,
                          contentCP, noContent, noContent, noContent, noContent, noContent,
                          contentQ, contentQ, contentQ, contentQ,
                          noContent, noContent};
/*
            //needed earlier when RAM was a problem, switched to progmem
const char* const title[] ={"MPB", "UM", "UCC", "UCX", "UCn", "UR", "UCH",
                                           "DM", "DCC", "DCX", "DCn", "DR", "DCH", "YM",
                                           "RM", "RCC", "RCX", "RCn", "RR", "RCH",
                                           "LM", "LCC", "LCX", "LCn", "LR", "LCH", "XM",
                                           "SM", "SCC", "SCX", "SCn", "SR", "SCH",
                                           "SvP", "LdP"};

const char* const content[menuMax+1][3] PROGMEM={{"\0"},
                         {"Ct", "PB"}, {"\0"}, {"\0"}, {"\0"}, {"\0"}, {"\0"},
                         {"Ct", "PB"}, {"\0"}, {"\0"}, {"\0"}, {"\0"}, {"\0"},
                         {"Sp", "CJ"},
                         {"Ct", "PB"}, {"\0"}, {"\0"}, {"\0"}, {"\0"}, {"\0"},
                         {"Ct", "PB"}, {"\0"}, {"\0"}, {"\0"}, {"\0"}, {"\0"},
                         {"Sp", "CJ"},
                         {"Ct", "PB"}, {"\0"}, {"\0"}, {"\0"}, {"\0"}, {"\0"},
                         {"\0"}, {"\0"}};

*/
