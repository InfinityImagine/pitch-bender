#include "PitchBendConstants.h"

#define Mode 0
#define CC 1
#define CCMX 2
#define CCMN 3
#define Range 4
#define Chan 5

class Data
{
      public:
      int mode, cont, limH, limL, ran, chan, val, len;
};

/*
Data makeDataX() //copy this inline
{
    Data dX;
    int buff = analogRead(xPin);
    dX.val = map(buff, xMin, xMax, -8192, 8191);
    if(dX.val<0) dX.base = leftMode;
    else dX.base = rightMode;
    dX = (Data){dX.base, valueState[base + CC], valueState[base + CCMN], valueState[base + CCMX], valueState[base + Range], valueState[base + Chan], dX.val, xCont};
}
*/

int getQuad(Data dX, Data dY, int* valueState) //again, inlined, so the pointer is irrelevant
{
      if(dY.val>0)
            if(dX.val>0)
                  return valueState[quad1];
            else return valueState[quad2];
      else if(dX.val>0)
            return valueState[quad4];
      else return valueState[quad3];
}

/*
Data ellipse(Data dX, Data dY)
{
    int tn = dY.val/dX.val;
    int cos2 = 1/(tn*tn+1);
    int sin2 = 1 - cos2;
    int r = sqrt(dX.val*dX.val+dY.val*dY.val);
    int dist = dY.ran*dY.ran*cos2*cos2*(dX>0?1:-1)+dX.ran*dX.ran*sin2*sin2*(dY>0?1:-1);
    dist = dist<0 ? -sqrt(-dist) : sqrt(dist);
    dist = dY.ran*dX.ran/dist;
    dist = dist*r/valueState[maxRange];
    dX.val=dist;
    return dX;
}
*/

/* //it'll be inlined so forget the by reference problem
void processData(Data &dX)
{
    if(dX.mode == pitchbend)
    {
        dX.val *= dX.ran;
        dX.val /= valueState[maxRange];
    }
    else if(dX.mode == controller)
    {
        int toSend = (dX.val + dX.len<<14)>>(6+dX.len);
        toSend = dX.min + (toSend*(dX.max-dX.min))>>7;
    }
}
*/

/*
void sendData(Data dX)
{
    //to do
}
*/
