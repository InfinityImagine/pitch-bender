#include <MIDI.h>
#include <LiquidCrystal.h>
//#include <NewPing.h>
#include <avr/pgmspace.h>

#include "PitchBendConstants.h"
#include "PitchBendStrings.h"
#include "PitchBendStruct.h"

MIDI_CREATE_DEFAULT_INSTANCE();
//NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);
LiquidCrystal lcd(RSPin, EnablePin, D4Pin, D5Pin, D6Pin, D7Pin);

char buffer[30];
int Max[menuMax+1]={
                     23, //maxRange 0

                     1, //upMode 1
                     127, //upCC 2
                     127, //upCCMX 3
                     126, //upCCMN 4
                     23, //upRange 5
                     15, //upChan 6

                     1, //lowMode 7
                     127, //lowCC 8
                     127, //lowCCMX 9
                     126, //lowCCMN 10
                     23, //lowRange 11
                     15, //lowChan 12

                     1, //yCont 13

                     1, //rightMode 14
                     127, //rightCC 15
                     127, //rightCCMX 16
                     126, //rightCCMN 17
                     23, //rightRange 18
                     15, //rightChan 19

                     1, //leftMode 20
                     127, //leftCC 21
                     127, //leftCCMX 22
                     126, //leftCCMN 23
                     23, //leftRange 24
                     15, //leftChan 25

                     1, //xCont 26

                     1, //dlMode 27
                     127, //dlCC 28
                     127, //dlCCMX 29
                     126, //dlCCMN 30
                     23, //dlRange 31
                     15, //dlChan 32

                     1, //quad1 33
                     1, //quad2 34
                     1, //quad3 35
                     1, //quad4 36
                     
                     19, //save 37
                     19 //load 38
                   };

int xMax=500, xMin=500, yMax=500, yMin=500, xMean, yMean, buff=0;
int xVal=0, yVal=0, dlVal=0;
int xPre=-1, yPre=-1, dlPre=-1;
int menuState=0, preMenu = -1;

//int valueState[]={11, 1, 6, 0, 1, 1, 0, 0, 1, 1, 0, 1, 11, 0, 0, 1, 6, 0, 0, 0}; //old, ignore
int valueState[menuMax+1]={
                            1, //maxRange 0

                            pitchbend, //upMode 1
                            15, //upCC 2
                            127, //upCCMX 3
                            0, //upCCMN 4
                            1, //upRange 5
                            0, //upChan 6

                            pitchbend, //lowMode 7
                            16, //lowCC 8
                            127, //lowCCMX 9
                            0, //lowCCMN 10
                            1, //lowRange 11
                            0, //lowChan 12

                            split, //yCont 13

                            pitchbend, //rightMode 14
                            17, //rightCC 15
                            127, //rightCCMX 16
                            0, //rightCCMN 17
                            1, //rightRange 18
                            0, //rightChan 19

                            pitchbend, //leftMode 20
                            18, //leftCC 21
                            127, //leftCCMX 22
                            0, //leftCCMN 23
                            1, //leftRange 24
                            0, //leftChan 25

                            split, //xCont 26

                            controller, //dlMode 27
                            29, //dlCC 28
                            127, //dlCCMX 29
                            0, //dlCCMN 30
                            1, //dlRange 31
                            0, //dlChan 32

                            ellipse, //quad1 33
                            ellipse, //quad2 34
                            ellipse, //quad3 35
                            ellipse, //quad4 36

                            0, //save 37
                            0 //load 38
                          };

int preValue[menuMax+1]={ 
                          -1,
                          -1, -1, -1, -1, -1, -1,
                          -1, -1, -1, -1, -1, -1,
                          -1,
                          -1, -1, -1, -1, -1, -1,
                          -1, -1, -1, -1, -1, -1,
                          -1,
                          -1, -1, -1, -1, -1, -1,
                          -1, -1,
                          -1, -1, -1, -1
                        };

int chanMsg[16]={
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0
                };

int chanPre[16]={
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0,
                  0, 0, 0, 0
                };

int preButton[5] = {0, 0, 0, 0, 0};

int isPress(int pin)
{
  int ret=0;
  if(pin>12) pin=12;
  int reading = digitalRead(pin);
  if (preButton[pin-rightPin]==LOW && reading==HIGH)
                    ret= 1;
  preButton[pin-rightPin] = reading;
  return ret;
}

/*
int pbGetX(int aXis)
{
//      xVal = map(buff, xMin, xMax, -8192, 8191);
      int temp=0;
      if(aXis>0) temp=valueState[rightMode];
      else if(aXis<0) temp=valueState[leftMode];
      temp *= aXis;
      return temp;
}

int pbGetY(int aYis)
{
//      yVal = map(buff, yMin, yMax, -8192, 8191);
      int temp=0;
      if(aYis>0) temp=valueState[upMode];
      else if(aYis<0) temp=valueState[lowMode];
      temp *= -aYis;
      return temp;
}
*/

void setup()
{
      MIDI.begin(MIDI_CHANNEL_OMNI);
      Serial.begin(115200);
      lcd.begin(16, 2);
      pinMode(upPin, INPUT);
      pinMode(downPin, INPUT);
      pinMode(leftPin, INPUT);
      pinMode(rightPin, INPUT);
      pinMode(buttonPin, INPUT);
      lcd.print("PITCH BENDER M-2");
      lcd.setCursor(0, 1);
      lcd.print("<press up...>");
      while(!isPress(upPin));
      lcd.clear();
      lcd.print("CALIBRATE");
      lcd.setCursor(0, 1);
      lcd.print("JOYSTICK");
      while(!isPress(upPin))
      {
          if((xMean=analogRead(xPin))>xMax) xMax=analogRead(A0); //? why not just xMax=xMean??
          else if(xMean<xMin) xMin=analogRead(A0);
          if((yMean=analogRead(yPin))>yMax) yMax=analogRead(A1);
          else if(yMean<yMin) yMin=analogRead(A1);
      }
      ++xMax;
      --xMin;
      ++yMax;
      //--yMin;
      lcd.clear();
}

void loop()
{
      //Buttons 
      if(isPress(leftPin) == 1)
      {
            --valueState[menuState];
            if(valueState[menuState]<0) valueState[menuState]=0;
      }
      if(isPress(rightPin) == 1)
      {
            ++valueState[menuState];
            if(valueState[menuState]>Max[menuState]) valueState[menuState]=Max[menuState];
      }
      if(valueState[menuState]!=preValue[menuState])
      {
          switch(menuState)
          {
              case maxRange:
              //Max range update.
              Max[upRange]=Max[lowRange]=Max[rightRange]=Max[leftRange]=Max[dlRange]=valueState[maxRange];
              if(valueState[upRange]>Max[upRange]) valueState[upRange]=Max[upRange];
              if(valueState[lowRange]>Max[lowRange]) valueState[lowRange]=Max[lowRange];
              if(valueState[rightRange]>Max[rightRange]) valueState[rightRange]=Max[rightRange];
              if(valueState[leftRange]>Max[leftRange]) valueState[leftRange]=Max[leftRange];
              if(valueState[dlRange]>Max[dlRange]) valueState[dlRange]=Max[dlRange];
              break;
              
              case upCCMX:
              Max[upCCMN]=valueState[upCCMX]-1;
              valueState[upCCMN]%=valueState[upCCMX];
              break;
              
              case lowCCMX:
              Max[lowCCMN]=valueState[lowCCMX]-1;
              valueState[lowCCMN]%=valueState[lowCCMX];
              break;
              
              case rightCCMX:
              Max[rightCCMN]=valueState[rightCCMX]-1;
              valueState[rightCCMN]%=valueState[rightCCMX];
              break;
              
              case leftCCMX:
              Max[leftCCMN]=valueState[leftCCMX]-1;
              valueState[leftCCMN]%=valueState[leftCCMX];
              break;
              
              case dlCCMX:
              Max[dlCCMN]=valueState[dlCCMX]-1;
              valueState[dlCCMN]%=valueState[dlCCMX];
              break;
          }
      }
      if(isPress(upPin) == 1)
      {
            ++menuState;
            if(menuState>menuMax) menuState=0;
      }
      if(isPress(downPin) == 1)
      {
            --menuState;
            if(menuState<0) menuState=menuMax;
      }
      //lcd print
      if(preMenu != menuState || preValue[menuState] != valueState[menuState])
      {
            preMenu=menuState;
            preValue[menuState]=valueState[menuState];
            lcd.clear();
            strcpy_P(buffer, (char*)pgm_read_word(&(title[menuState])));
            lcd.print(buffer);
            lcd.setCursor(0, 1);
            strcpy_P(buffer, (char*)pgm_read_word(content[menuState]));
            if(buffer[0]!='X')
            {
                strcpy_P(buffer, (char*)pgm_read_word(&(content[menuState][valueState[menuState]])));
                lcd.print(buffer);
            }
            else lcd.print(valueState[menuState]+1);
      }

      //input
      buff = analogRead(xPin);
      if(buff<xMean) xVal = map(buff, xMin, xMean, -8192, 0);
      else xVal = map(buff, xMean, xMax, 0, 8191);
      //xVal=buff*16-8192;

      buff = analogRead(yPin);
      //yVal *=-1; //strange error,  ignore for now
      if(buff<yMean) yVal = map(buff, yMin, yMean, -8192, 0);
      else yVal = map(buff, yMean, yMax, 0, 8191);
      yVal = map(buff, yMin, yMax, -8192, 8191);
      //yVal=buff*16-8192;

      //buff = sonar.ping()/US_ROUNDTRIP_CM;
      //if(buff<5 || buff>50) buff = 5;
      //dlVal = map(buff, 5, 48, 0, 8191);

      //pitch bend    //WORKING but change to ELLIPSE/DIAMOND
      for(int i=0; i<16; ++i)
      {
          chanMsg[i]=0;
          if(valueState[upChan]==i && yVal>0)
                chanMsg[i] += yVal*(valueState[upRange]+1)*valueState[upMode];
          if(valueState[lowChan]==i && yVal<0)
                chanMsg[i] += yVal*(valueState[lowRange]+1)*valueState[lowMode];
          if(valueState[rightChan]==i && xVal>0)
                chanMsg[i] += xVal*(valueState[rightRange]+1)*valueState[rightMode];
          if(valueState[leftChan]==i && xVal<0)
                chanMsg[i] += xVal*(valueState[leftRange]+1)*valueState[leftMode];
          //if(valueState[dlChan]==i)
            //    chanMsg[i] += dlVal*(valueState[dlRange]+1)*valueState[dlMode];

          chanMsg[i]/=(valueState[maxRange]+1);
          //if(xVal!=xPre || yVal!=yPre || dlVal!=dlPre)
          if(chanMsg[i]!=chanPre[i])
          { 
               MIDI.sendPitchBend(chanMsg[i], i+1); 
               chanPre[i]=chanMsg[i];
          }
          else MIDI.sendNoteOff(0, 0, 0); //dummy 
      }


/*      //pitch bend ellipse/diamond... incorrect formula
      for(int i=0; i<16; ++i)
      {
          chanMsg[i]=0;
          int x, y, d;
          //x/=(valueState[0]+1);
          //y/=(valueState[0]+1);
          if(valueState[3]==i && yVal>0)
          {
                y = yVal*(valueState[2]+1)*valueState[1];
                chanMsg[i] += y*y;
          }
          else if(valueState[6]==i && yVal<0)
          {
                y = yVal*(valueState[5]+1)*valueState[4];
                chanMsg[i] -= y*y;
          }
          if(valueState[10]==i && xVal>0)
          {
                x = xVal*(valueState[9]+1)*valueState[8];
                chanMsg[i] += x*x;
          }
          else if(valueState[13]==i && xVal<0)
          {
                xVal*(valueState[12]+1)*valueState[11];
                chanMsg[i] -= x*x;
          }
          if(valueState[17]==i)
          {
                d = dlVal*(valueState[16]+1)*valueState[15];
                chanMsg[i] += d*d;
          }
          if(chanMsg[i]<0)     
            chanMsg[i]=-(int)sqrt(-chanMsg[i]);
          else chanMsg[i]=(int)sqrt(chanMsg[i]);
          chanMsg[i]/=(valueState[0]+1);
          if(xVal!=xPre || yVal!=yPre || dlVal!=dlPre)
          //if(chanMsg[i]!=chanPre[i])
          { 
               MIDI.sendPitchBend(chanMsg[i], i+1); 
               chanPre[i]=chanMsg[i];
          }
      }
*/

      //controller changes     //WORKING  //ADDed upCCMX upCCMN usage, check it!! //replace mult/div by left/right shift
      for(int i=0; i<16; ++i)
      {
          if(yVal!=yPre)
          {
              if(valueState[upChan]==i && valueState[upMode]==controller && (valueState[yCont]==common || yVal>0))
              {
                  int toSend = (yVal + 8191*valueState[yCont])/(64*(1+valueState[yCont]));
                  toSend = valueState[upCCMN] + (double)toSend*(valueState[upCCMX]-valueState[upCCMN])/128;
                  MIDI.sendControlChange(valueState[upCC]+1, toSend, i);
                  MIDI.sendControlChange(valueState[upCC]+1, toSend, i+1); //dummy
              }
              if(valueState[lowChan]==i && valueState[lowMode]==controller && valueState[yCont]==split && yVal<0)
              {
                  int toSend = (yVal+8191)/64;
                  toSend = valueState[lowCCMN] + (double)toSend*(valueState[lowCCMX]-valueState[lowCCMN])/128;
                  MIDI.sendControlChange(valueState[lowCC]+1, toSend, i);
                  MIDI.sendControlChange(valueState[lowCC]+1, toSend, i+1); //dummy
              }
              //else MIDI.sendNoteOff(0, 0, 0);  //dummy
          }
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          if(xVal!=xPre)
          {     
              if(valueState[rightChan]==i && valueState[rightMode]==0 && (valueState[xCont]==1 || xVal>0))
              {
                  int toSend = (xVal + 8191*valueState[xCont])/(64*(1+valueState[xCont]));
                  toSend = valueState[rightCCMN] + (double)toSend*(valueState[rightCCMX]-valueState[rightCCMN])/128;
                  MIDI.sendControlChange(valueState[rightCC]+1, toSend, i);
                  MIDI.sendControlChange(valueState[rightCC]+1, toSend, i+1); //dummy
              }
              if(valueState[leftChan]==i && valueState[leftMode]==0 && valueState[xCont]==0 && xVal<0)
              {
                  int toSend = (xVal+8191)/64;
                  toSend = valueState[leftCCMN] + (double)toSend*(valueState[leftCCMX]-valueState[leftCCMN])/128;
                  MIDI.sendControlChange(valueState[leftCC]+1, toSend, i);
                  MIDI.sendControlChange(valueState[leftCC]+1, toSend, i+1); //dummy
              }
              //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          }
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
          /*if(dlVal!=dlPre && valueState[dlMode]==0 && valueState[dlChan]==i)
          {
                MIDI.sendControlChange(valueState[dlCC]+1, dlVal/128, i);
                MIDI.sendControlChange(valueState[dlCC]+1, dlVal/128, i+1); //dummy
          }*/
          //else MIDI.sendNoteOff(0, 0, 0); //dummy 
      }

      xVal=xPre;
      yVal=yPre;
      dlVal=dlPre;
}

/*
void loop()          //basic final outline
{
      menuUpdate();

      Data dX;
      buff = analogRead(xPin);
      dX.val = map(buff, xMin, xMax, -8192, 8191);
      if(dX.val<0) dX.base = leftMode;
      else dX.base = rightMode;
      dX = (Data){dX.base, valueState[base + CC], valueState[base + CCMN], valueState[base + CCMX], valueState[base + Range], valueState[base + Chan], dX.val, valueState[xCont]};

      Data dY;
      buff = analogRead(yPin);
      dY.val = map(buff, yMin, yMax, -8192, 8191);
      if(dY.val<0) dY.base = lowMode;
      else dY.base = upMode;
      dY = (Data){dY.base, valueState[base + CC], valueState[base + CCMN], valueState[base + CCMX], valueState[base + Range], valueState[base + Chan], dY.val, valueState[yCont]};

      Data dZ;
      buff = analogRead(dlPin);
      dZ.val = map(buff, dlMin, dlMax, 0, 8191);
      dZ.base = dlMode;
      dZ = (Data){dZ.base, valueState[base + CC], valueState[base + CCMN], valueState[base + CCMX], valueState[base + Range], valueState[base + Chan], dZ.val, 0};

      if(dX.mode==pitchBend && dY.mode==pitchBend && dX.chan==dY.chan)
            switch(getQuad(dX, dY, valueState))
            {
                case diamond:
                    sendData(diamond(dX, dY));
                break;
                case ellipse:
                    sendData(ellipse(dX, dY));
                break;
            }
      else
      {
            process(dX);
            process(dY);
            sendData(dX);
            sendData(dY);
      }
      process(dZ);
      sendData(dZ);
       xVal=xPre;
      yVal=yPre;
      dlVal=dlPre;
}
*/
