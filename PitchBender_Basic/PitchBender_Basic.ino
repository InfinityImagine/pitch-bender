#include <MIDI.h>

MIDI_CREATE_DEFAULT_INSTANCE();

int xVal, yVal, xPre, yPre;
double xPin=A0, yPin=A1, buff;

void setup()
{
    MIDI.begin(12);
   Serial.begin(115200); 
   xPre = yPre=-1;
}

void loop()
{
    buff = analogRead(xPin);
    xVal = map(buff, 0, 1023, -8191, 8191);
    buff = analogRead(yPin);
    yVal = map(buff, 0, 1023, -8191,8191);
    if(xPre!=xVal)
    {
      MIDI.sendPitchBend(xVal+yVal, 12);
      xPre=xVal;
    }
}

