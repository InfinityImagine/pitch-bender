#include <LiquidCrystal.h>
//LiquidCrystal lcd(5, 4, 3, 2, 1, 0);
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
const int leftPin = 0;
const int rightPin = 1;
const int upPin = 6;
const int downPin = 7;
const char* title[]={"Max P.B. Range", "Upper Mode", "Upper Range", "Upper Channel",
                "Lower Mode", "Lower Range", "Lower Channel", "X Controller",
                "Right Mode", "Right Range", "Right Channel",
                "Left Mode", "Left Range", "Left Channel", "Y Controller",
                "D-Light Mode", "D-Light Range", "D-Light Channel",
                "Save Preset", "Load Preset"};
const char* content[20][14]={{"\0"},
                         {"0. Pitch Bend", "1. Controller"}, {"\0"}, {"\0"},
                         {"0. Pitch Bend", "1. Controller"}, {"\0"}, {"\0"},
                         {"0. Split", "1. Common"},
                         {"0. Pitch Bend", "1. Controller"}, {"\0"}, {"\0"},
                         {"0. Pitch Bend", "1. Controller"}, {"\0"}, {"\0"},
                         {"0. Split", "1. Common"},
                         {"0. Pitch Bend", "1. Controller"}, {"\0"}, {"\0"},
                         {"\0"}, {"\0"}};
int Max[]={19, 23, 1, 23, 15, 1, 23, 15, 1, 1, 23, 15, 1, 23, 15, 1, 1, 23, 15, 20, 20};

double buff;
int xVal, yVal, dlVal;
int xPre, yPre, dlPre;
int menuState=0, preMenu = -1;
int valueState[]={1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0};
int preValue[20]={-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
int channel[5]={0, 0, 0, 0, 0};
int chanMsg[16]={0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

int preButton[5] = {0, 0, 0, 0, 0};

int isPress(int pin)
{
  int ret=0;
  int reading = digitalRead(pin);
  if (reading != preButton[pin-6] && reading==HIGH)
                    ret= 1;
  preButton[pin-6] = reading;
  return ret;
}


void setup()
{
      Serial.begin(9600);
      lcd.begin(16, 2);
      lcd.clear();
      pinMode(upPin, INPUT);
      pinMode(downPin, INPUT);
      pinMode(leftPin, INPUT);
      pinMode(rightPin, INPUT);
      lcd.print("hello, world!");
}

void loop()
{
      //Buttons 
      if(isPress(leftPin) == 1)
      {
            --valueState[menuState];
            if(valueState[menuState]<0) valueState[menuState]=0;
      }
      if(isPress(rightPin) == 1)
      {
            ++valueState[menuState];
            if(valueState[menuState]>Max[menuState+1]) valueState[menuState]=Max[menuState+1];
      }
      if(menuState==0 && valueState[menuState]!=preValue[menuState])
            Max[3]=Max[6]=Max[10]=Max[13]=Max[17]=valueState[0]; //Max range update
      if(isPress(upPin) == 1)
      {
            ++menuState;
            if(menuState>Max[0]) menuState=Max[0];
      }
      if(isPress(downPin) == 1)
      {
            --menuState;
            if(menuState<0) menuState=0;
      }
      //lcd print
      if(preMenu != menuState || preValue[menuState] != valueState[menuState])
      {
            preMenu=menuState;
            preValue[menuState]=valueState[menuState];
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print(title[menuState]);
            lcd.setCursor(0, 1);
            if(content[menuState][0]!="\0") lcd.print(content[menuState][valueState[menuState]]);
            else lcd.print(valueState[menuState]+1);
      }
}
