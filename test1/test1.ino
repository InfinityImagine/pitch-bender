#include <LiquidCrystal.h>
#include <avr/pgmspace.h>
LiquidCrystal lcd(7, 6, 5, 4, 3, 2);
int x, y, z;

const char hi[] PROGMEM = "hello";
const char bye[] PROGMEM = "world";
const char* const msg[] PROGMEM = {hi, bye};
char buffer[10];

void setup()
{
   lcd.begin(16, 2);
}

void loop()
{
   lcd.clear();
   strcpy_P(buffer, (char*)pgm_read_word(&(msg[0])));
   lcd.print(buffer);
   lcd.setCursor(0, 1);
   strcpy_P(buffer, (char*)pgm_read_word(&(msg[1])));
   lcd.print(buffer);
   exit(1);
   
}
