#include <NewPing.h>

#define TRIGGER_PIN  18 
#define ECHO_PIN     19 
#define MAX_DISTANCE 50

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

void setup()
{
  Serial.begin(9600);
}

void loop()
{
  long duration, Dval, cm;
  duration = sonar.ping();
  cm = duration/US_ROUNDTRIP_CM;
  Dval = map(cm, 5, 48, 0, 1023);
  if(Dval<0 || Dval>1024) Dval =0;
  Serial.print(cm);
  Serial.println("cm");
  Serial.print(Dval);
  Serial.println(" value");
}
